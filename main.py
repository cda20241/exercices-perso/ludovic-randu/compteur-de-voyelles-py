

def get_voyelles(reponse):
    nbr_voyelles = 0
    list_voyelles = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E','I', 'O', 'U', 'Y'
                     'â', 'ê', 'î', 'ô', 'û', 'Â', 'Ê','Î', 'Ô', 'Û'
                     'ä', 'ë', 'ï', 'ö', 'ü', 'ÿ', 'Ä', 'Ë','Ï', 'Ö', 'Ü'
                     'à', 'é', 'è', 'ù']
    for letter in reponse :
        if letter in list_voyelles:
            nbr_voyelles += 1
    return nbr_voyelles

reponse = input("Saisissez une phrase:")
compt_voyelles = get_voyelles(reponse)
print('il y a '+ str(compt_voyelles) + ' voyelles')
